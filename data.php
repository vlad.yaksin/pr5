<?php
<?php

class NumApi
{
    const BASE_URL = "http://numbersapi.com/";

    public static function getNum()
    {
        $rate = self::getRequestNum();
        return "<div class=\"math\">API say: {$rate}</div>";
    }


    private function getRequestNum() 
    {
        $url = self::buildUrl("random/math");
        $response = file_get_contents($url);
        return $response;
    }

    private function buildUrl(string $params) 
    {
        return self::BASE_URL . $params;
    }

}