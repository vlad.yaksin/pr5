<?php
/*
Plugin Name: Fact
Description: Number Fact
Version: 1.0
Author: Vladislav
Author URI: https://t.me
*/

defined( 'ABSPATH' ) || die();
define('NUM_DIR', dirname( __FILE__ ));

require_once NUM_DIR . "/data.php";
require_once NUM_DIR . "/shortcode.php";